<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\UserController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/** normal cities api witout security */
// Route::ApiResource('/cities',CityController::class);

/** Normal route apis */
// Route::get('/cities', [CityController::class, 'index']);
// Route::post('/cities', [CityController::class, 'store']);
// Route::get('/cities/{id}', [CityController::class, 'show']);
// Route::put('/cities/{city}', [CityController::class, 'update']);
// Route::delete('/cities/{city}', [CityController::class, 'destroy']);

/** normal weather api witout security */
// Route::get('/weather/{city}', [CityController::class, 'show']);
// Route::get('/weather', [CityController::class, 'indexWeather']);

// api secure using sanctum auth
Route::post('login',[UserController::class,'loginUser']);

Route::group(['middleware' => 'auth:sanctum'],function(){
    Route::get('user',[UserController::class,'userDetails']);
    Route::get('logout',[UserController::class,'logout']);

    Route::ApiResource('/cities',CityController::class);

    /** Get all City weathers */
    Route::get('/weather/{city}', [CityController::class, 'show']);
    Route::get('/weather', [CityController::class, 'indexWeather']);
});