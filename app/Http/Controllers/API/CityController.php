<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\City;

class CityController extends Controller
{

    // get all cities
    public function index()
    {
        $cities = City::all();

        return response()->json($cities);
    }

    // store city using api
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'country' => 'required|string|max:255',
        ]);

        $city = new City();
        $city->name = $request->name;
        $city->country = $request->country;
        $city->save();

        return response()->json([
            "success" => true,
            "message" => "City added Successfully",
            "data" => $city,
        ]);

    }
    
    // update city
    public function update(Request $request, City $city){
        
        $request->validate([
            'name' => 'required|string|max:255',
            'country' => 'required|string|max:255',
        ]);

        $city->name = $request->name;
        $city->country = $request->country;
        $city->update();

        return response()->json([
            "success" => true,
            "message" => "City Updated Successfully",
            "data" => $city,
        ]);
    }

    // show city weathers
    public function show($id)
    {
        $city = City::findOrFail($id);

        $response = Http::get("https://api.openweathermap.org/data/2.5/forecast?q={$city->name},{$city->country}&appid=" . env('OPEN_WEATHER_API_KEY'));

        if ($response->failed()) {
            return response()->json(['error' => 'Failed to fetch weather data'], 500);
        }

        $weatherData = $response->json();

        return response()->json($weatherData);
    }

    // show all city weather is stored in database
    public function indexWeather()
    {
        
        $cities = City::all();
        $weatherData = [];

        foreach ($cities as $city) {
            $response = Http::get("https://api.openweathermap.org/data/2.5/forecast?q={$city->name},{$city->country}&appid=" . env('OPEN_WEATHER_API_KEY'));
            $weatherData[$city->id] = $response->json();
        }

        if ($response->failed()) {
            return response()->json(['error' => 'Failed to fetch weather data'], 500);
        }

        return response()->json($weatherData);
    }

    // delete a city
    public function destroy(City $city)
    {
        $city->delete();

        return response()->json([
            "success" => true,
            "message" => "City deleted successfully.",
            "data" => $city
        ]);
    }
}
