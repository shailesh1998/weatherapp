<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cities = [
            ['name' => 'navsari', 'country' => 'india'],
            ['name' => 'baroda', 'country' => 'india'],
            ['name' => 'valsad', 'country' => 'india'],
            ['name' => 'surat', 'country' => 'india'],
            ['name' => 'bhavnagar', 'country' => 'india'],
        ];

        foreach ($cities as $city) {
            DB::table('cities')->insert($city);
        }
    }
}
