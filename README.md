# Weather APIs Laravel


## Getting started

update .env add database name

1 install composer

- composer install

2 run database migration

- php artisan migrate

3 run database seeder for add a demo city data in table

- php artisan db:seed
- php artisan db:seed --class=CitiesTableSeeder


## Secure APIs With Sanctum Auth Security

##### 1 first login User 

Method:- POST
URL :- http://127.0.0.1:8000/api/login

Form Data 

- email - admin@mail.com
- password - 123

- check genrated tokan this tokan is used for access all APIs using secure way

##### 2 For Check User

Method:- POST
URL :- http://127.0.0.1:8000/api/user

pass tokan to get data

##### 3 Logout

Method:- POST
URL :- http://127.0.0.1:8000/api/logout


### City APIs

#### 1 Get all city 

Method:- GET
URL :- http://127.0.0.1:8000/api/cities/

#### 2 Add City 

Method:- POST
URL :- http://127.0.0.1:8000/api/cities/


#### 3 Update City 

Method:- PUT
URL :- http://127.0.0.1:8000/api/cities/{id}

#### 4 Delete City 

Method:- DELETE
URL :- http://127.0.0.1:8000/api/cities/{id}


### Weather APIs

#### Get all city Weathers

Method:- GET
URL :- http://127.0.0.1:8000/api/weather


#### Get Single city Weathers

Method:- GET
URL :- http://127.0.0.1:8000/api/weather/{id}


#### or view single city

Method:- GET
URL :- http://127.0.0.1:8000/api/cities/{id}
